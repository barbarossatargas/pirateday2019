import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'

import VueCarousel from 'vue-carousel'
Vue.use(VueCarousel)

import lightbox from 'vlightbox'
Vue.use(lightbox)

import vueSmoothScroll from 'vue2-smooth-scroll'
Vue.use(vueSmoothScroll)

Vue.config.productionTip = false

new Vue({
    render: h => h(App),
}).$mount('#app')